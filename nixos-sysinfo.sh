function nixos-sysinfo {
	echo "* System: $(nixos-version)"
	echo "* Nix version: $(nix-env --version)"
	echo "* Nixpkgs version: $(nix-instantiate --eval '<nixpkgs>' -A lib.nixpkgsVersion)"
	echo "* Sandboxing enabled: $(grep build-use-sandbox /etc/nix/nix.conf | grep -oE 'true|false')"
}
